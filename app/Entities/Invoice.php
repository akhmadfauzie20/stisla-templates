<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoices';

    protected $fillable = [
        'order_no',
        'total_price',
        'status',
        'order_date',
    ];

    protected $dates = [
        'order_date',
        'created_at',
        'updated_at',
    ];

    public function item()
    {
        return $this->hasMany(Item::class, 'invoice_id', 'id');
    }
}
