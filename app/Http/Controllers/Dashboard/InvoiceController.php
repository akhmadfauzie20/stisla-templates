<?php

namespace App\Http\Controllers\Dashboard;

use App\Entities\Invoice;
use App\Entities\Item;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function index(Request $request)
    {
        $invoice_query = Invoice::query();
        $status = $request->status;
        $q = $request->q;

        if (isset($q)) {
            $invoice_query->where('order_no', 'LIKE',"%$q%")
                          ->orWhere('total_price','LIKE', "%$q%")
                          ->orWhere('order_date','LIKE', "%$q%");
        }

        if (isset($status)) {
            $invoice_query->where('status', $status);
        }

        if ($request->all() != []) {
            $invoices = $invoice_query->paginate(25);
        } else {
            $invoices = Invoice::query()->paginate(25);
        }

//        dd($request->all() != []);


        return view('pages.invoice', [
            'invoices' => $invoices
        ]);
    }

    public function form($id = null)
    {
        $data['invoice'] = Invoice::query()->find($id);

        if ($data == 0) {
            $data['items'] = Item::query()->whereNull('invoice_id')->get();
        } else {
            $data['items'] = Item::query()->where('invoice_id', $id)->paginate(25);
        }

        return view('pages.invoice-form', $data);
    }

    public function create(Request $request)
    {
        $total = 0;
        $items = Item::query()->whereNull('invoice_id')->get();
        $invoice = new Invoice();

        foreach ($items as $item) {
            $invoice->order_no = sprintf("%04d", rand(1, 9));

            if ($item->qty > 1) {
                $total += $item->price * $item->qty;
            } else {
                $total += $item->price;
            }

            $invoice->total_price = $total;
            $invoice->status = 'WAIT_PAYMENT';
            $invoice->order_date = $request->date;
        }

        if ($invoice->save()) {
            $invoice->order_no = sprintf("%04d", $invoice->id);
            $invoice->save();

            foreach ($items as $item) {
                $item->invoice_id = $invoice->id;
                $item->save();
            }

            if (!empty($nullItems)) {
                foreach ($nullItems as $item) {
                    $item->invoice_id = $invoice->id;
                    $item->save();
                }
            }

            return redirect(route('dashboard.invoice.index'))->with('success', 'Invoice Berhasil Dibuat');
        }

        return redirect()->back()->with('error', 'Maaf, Ada Yang Salah Saat Membuat Invoice');
    }

    public function update(Request $request, $id)
    {
        $total = 0;
        $invoice = Invoice::query()->find($id);
        $items = Item::query()->where('invoice_id', $id)->get();
        $nullItems = Item::query()->whereNull('invoice_id')->get();

        if (!empty($nullItems)) {
            foreach ($nullItems as $item) {
                if ($item->qty > 1) {
                    $total += $item->price * $item->qty;
                } else {
                    $total += $item->price;
                }

                $invoice->total_price = $total;
            }
        }

        foreach ($items as $item) {
            if ($item->qty > 1) {
                $total += $item->price * $item->qty;
            } else {
                $total += $item->price;
            }

            $invoice->total_price = $total;
            $invoice->status = $request->status;
            $invoice->order_date = $request->date;
        }

        if ($invoice->save()) {

            foreach ($items as $item) {
                $item->invoice_id = $invoice->id;
                $item->save();
            }

            return redirect(route('dashboard.invoice.index'))->with('success', 'Invoice Berhasil Dibuat');
        }

        return redirect()->back()->with('error', 'Maaf, Ada Yang Salah Saat Membuat Invoice');
    }

    public function delete($id)
    {
        $invoice = Invoice::query()->find($id);

        if ($invoice->delete()) {
            $items = Item::query()->where('invoice_id', $id)->get();

            foreach ($items as $item) {
                $item->delete();
            }
        }

        return redirect()->back()->with('success', 'Invoice Berhasil Dihapus!');
    }

    public function invoice($id)
    {
        $data['invoice'] = Invoice::query()->find($id);

        $data['items'] = Item::query()
                             ->where('invoice_id', $id)
                             ->get();

        return view('pages.invoice-preview', $data);
    }
}
