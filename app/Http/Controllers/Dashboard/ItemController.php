<?php

namespace App\Http\Controllers\Dashboard;

use App\Entities\Item;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function create(Request $request)
    {
        $item = new Item();
        $item->title = $request->title;
        $item->price = $request->price;
        $item->qty = $request->qty;
        $item->save();

        return redirect()->back();
    }

    public function update(Request $request, $id)
    {
        $item = Item::query()->where('id', $id)->first();
        $item->title = $request->title;
        $item->price = $request->price;
        $item->qty = $request->qty;

        if ($item->save()) {
            return redirect()->back();
        }
    }

    public function delete($id)
    {
        $item = Item::query()->where('id', $id)->first();
        $item->delete();

        return redirect()->back();
    }
}
