<?php

use App\Entities\User;
use Illuminate\Database\Seeder;

class UserTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create(['username' => 'admin']);

//        factory(User::class, 14)->create();
    }
}
