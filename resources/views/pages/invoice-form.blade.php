@extends('scaffold')

<?php
    $title = @$invoice ? 'Update' : 'Create';
//    dd($invoice->order_date->format('m/d/Y'));
?>

@section('page-title', "$title Item Data")

@section('content-title-display', 'none')
@section('content-back-display', 'inline-block')
@section('content-back-link', route('dashboard.invoice.index'))

@section('content-breadcrumbs')
    @include('components.breadcrumb-item', ['text' => 'Invoice', 'active' => true, 'link' => route('dashboard.invoice.index') ])
    @include('components.breadcrumb-item', ['text' => "$title Invoice", 'active' => false])
@endsection
<style>
    .btn-edit {
        border-radius: 5px;
    }

    .btn-delete {
        margin-left: 5px;
        border-radius: 5px;
    }
</style>
@section('content')
    @if(session()->has('error'))
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> {{ session()->get('error') }}
        </div>
    @endif
    @if(session()->has('success'))
        <div class="alert alert-success">
            <i class="fa fa-check"></i> {{ session()->get('success') }}
        </div>
    @endif

    <div class="card">
        <div class="card-header">
            <h4>{{$title}} Item Data</h4>
        </div>
        <div class="card-body row">
            <div class="col-md-6 col-xs-12">
                <form action="{{ route('dashboard.item.create') }}" method="post">
                    @csrf {{ csrf_field() }} {{method_field('POST')}}
                    <div class="form-group">
                        <label for="title" class="form-control-label">Title : </label>
                        <input type="text" class="form-control" id="title" name="title"
                               placeholder="Masukkan Judul Item" value="{{ old('title') }}" required>
                    </div>
                    <div class="form-group">
                        <label for="price" class="form-control-label">Price : </label>
                        <input type="text" class="form-control" id="price" name="price" onkeypress="return onlyNumber(event)"
                               placeholder="Masukkan Harga Item" value="{{ old('price') }}" required>
                    </div>
                    <div class="form-group">
                        <label for="price" class="form-control-label">Qty : </label>
                        <input type="text" class="form-control" id="price" name="qty" onkeypress="return onlyNumber(event)"
                               placeholder="Masukkan Jumlah Item" value="{{ old('qty') }}" required>
                    </div>
                    <div class="form-group">
                        <button type="submit"
                                class="btn btn-lg btn-primary"
                                style="float:right; margin-left: 10px;">Create Item
                        </button>
                        <a href="{{ route('dashboard.invoice.index') }}"
                           class="btn btn-lg btn-danger"
                           style="float:right;">Cancel
                        </a>
                    </div>
                </form>
            </div>
            <div class="col-md-6 col-xs-12" style="width:100%">
                <form action="{{ route(is_null($invoice) ? 'dashboard.invoice.create' : 'dashboard.invoice.update', ['id' => @$invoice->id]) }}" method="post">
                    @csrf {{ csrf_field() }} {{method_field('POST')}}
                    <div>
                        <table class="table table-md table-hover table-compact">
                            <thead>
                            <tr>
                                <th scope="col" width="1">No.</th>
                                <th scope="col">Title</th>
                                <th scope="col">Price</th>
                                <th scope="col">Qty</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($items as $item)
                                <tr>
                                    <td class="v-middle" scope="row">{{ $loop->iteration }}</td>
                                    <td class="v-middle text-small">{{ $item->title }}</td>
                                    <td class="v-middle text-small">Rp{{ number_format($item->price, 0,".", ".") }}</td>
                                    <td class="v-middle text-small">{{ $item->qty }}</td>
                                    <td class="v-middle">
                                        <div class="btn-group">
                                            <button class="btn btn-sm btn-primary btn-edit"><i
                                                    class="fas fa-pen"></i>
                                            </button>
{{--                                            <form action="#" method="post" style="margin-left: 12px;">--}}
{{--                                                {{ csrf_field() }} {{ method_field('DELETE') }}--}}
{{--                                                <button class="btn btn-sm btn-danger btn-delete" type="submit"--}}
{{--                                                        onclick="return confirm('Yakin ingin menghapus data ?')"><i--}}
{{--                                                        class="fas fa-trash"></i>--}}
{{--                                                </button>--}}
{{--                                            </form>--}}
                                        </div>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td class="text-center" colspan="8">No item yet.</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                        <div class="col-md-12 col-xs-12" style="margin-top: 16px;">
                            <div class="float-right">
                                {{ $items->appends(request()->except('page'))->links() }}
                            </div>
                        </div>
                        @if($items->count() > 0)
                            @if(!empty($invoice))
                                <div class="form-group">
                                    <label for="date" class="form-control-label">Date : </label>
                                    <input type="date" class="form-control daterangepicker" id="date" name="date"
                                           placeholder="Masukkan Tanggal Transaksi"  value="{{ old('date') ?: @$invoice->order_date->format('Y-m-d') }}" required>
                                </div>

                                <div class="form-group">
                                    <label for="status" class="form-control-label">Status : </label>
                                    <select class="custom-select custom-select-sm" name="status" id="status">
                                        <option value="{{ @$invoice->status }}" selected>{{ @$invoice->status == 'PAID' ? 'Dibayar' : 'Belum Dibayar' }}</option>
                                        <option value="PAID">Dibayar</option>
                                        <option value="WAIT_PAYMENT">Belum Dibayar</option>
                                    </select>
                                </div>
                            @else
                                <div class="form-group">
                                    <label for="date" class="form-control-label">Date : </label>
                                    <input type="date" class="form-control daterangepicker" id="date" name="date"
                                           placeholder="Masukkan Tanggal Transaksi" required>
                                </div>
                            @endif

                            <button type="submit"
                                    class="btn btn-lg btn-primary"
                                    style="float:right;">{{ $title }} Invoice
                            </button>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        // $(document).ready(function () {
        $(".select option").val(function (idx, val) {
            $(this).siblings('[value="' + val + '"]').remove();
            return val;
        });

        function onlyNumber(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))

                return false;
            return true;
        }

        $('#discount').on('change', function () {
            calcDiscount();
        });

        $('#price').on('change', function () {
            calcDiscount();
        });

        function calcDiscount() {
            let price = parseInt(document.getElementById("price").value);
            let discount = parseInt(document.getElementById("discount").value);

            let total = price - discount;

            if (price < discount) total = 0;

            // console.log(price);
            document.getElementById("total_price").value = Math.abs(total);
        }
    </script>
@endsection

