@extends('scaffold')
<?php
    $title = 'Invoice';
?>
@section('page-title', $title)
@section('content-title', $title)

@section('content-breadcrumbs')
    @include('components.breadcrumb-item', ['text' => $title, 'active' => false])
@endsection
@section('content')
    @if(session()->has('error'))
        <div class="alert alert-danger">
            <i class="fa fa-exclamation-triangle"></i> {{ session()->get('fail') }}
        </div>
    @endif
    @if(session()->has('success'))
        <div class="alert alert-success">
            <i class="fa fa-check"></i> {{ session()->get('success') }}
        </div>
    @endif

    <div class="card">
        <div class="card-header">
            <h4>{{ $title }}</h4>
            <form action="{{ route('dashboard.invoice.index', request()->all())}}" method="GET"
                  class="form-inline" style="margin-left: auto">
                <div class="card-header-form">
                    <div class="input-group">
                        <div class="input-group">
                            <input autocomplete="off" type="text" class="form-control" name="q"
                                   value="{{ app('request')->input('q', '') }}"
                                   placeholder="Search name , description , points etc."
                                   style="max-width: 150px; border-radius: 30px 0 0 30px !important; height: 32px; margin-top: -1px;">
                            <select class="custom-select custom-select-sm" name="status" style="display: inline-block;
                                    max-width: 128px;
                                    border-radius: 0 0 0 0
                                    !important; padding:1px 15px; height: 32px; margin-top: -1px;" id="statusFilter">
                                <option value="PAID">Dibayar</option>
                                <option value="WAIT_PAYMENT">Belum Dibayar</option>
                            </select>
                            <div class="input-group-btn">
                                <button class="btn btn-search btn-primary"
                                        style="border-radius: 0 30px 30px 0 !important; margin-right:20px;">Search
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="card-header-action">
                <a href="{{ route('dashboard.invoice.form') }}" class="btn btn-primary" style="margin-top: -2px;">Create Data&nbsp;&nbsp;&nbsp;&nbsp;<i class="fas fa-plus"></i></a>
{{--                @if(!empty(request()->session()->get('CHOOSE_PRODUCT')))--}}
{{--                    <a href="{{ route('dashboard.sales.choose-finish') }}" class="btn btn-success" style="margin-top: -2px;">Done&nbsp;&nbsp;&nbsp;&nbsp;<i class="fas fa-check"></i></a>--}}
{{--                @endif--}}
            </div>
        </div>
        <table class="table table-md table-hover table-compact">
            <thead>
            <tr>
                <th scope="col" width="1">No.</th>
                <th scope="col">Invoice No.</th>
                <th scope="col">Date Transaction</th>
                <th scope="col">Total Price</th>
                <th scope="col">Status</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @forelse($invoices as $invoice)
                <tr>
                    <td class="v-middle" scope="row">{{ $loop->iteration }}</td>
                    <td class="v-middle text-small">{{ $invoice->order_no }}</td>
                    <td class="v-middle text-small">{{ $invoice->order_date->format('Y-m-d') }}</td>
                    <td class="v-middle text-small">Rp {{ number_format($invoice->total_price, 0,".", ".") }}</td>
                    <td class="v-middle text-small">{{ $invoice->status == 'PAID' ? 'Dibayar' : 'Belum Dibayar' }}</td>
                    <td class="v-middle">
                        <div class="btn-group">
                            <a href="{{ route('dashboard.invoice.form', ['id' => $invoice->id]) }}" class="btn btn-sm btn-primary" style="border-radius: 5px;"><i class="fas fa-pen"></i></a>
                            <a href="{{ route('dashboard.invoice.print', ['id' => $invoice->id]) }}" class="btn btn-sm btn-success"
                              target="_blank" style="border-radius: 5px; margin-left: 12px;"><i class="fas fa-file-invoice"></i></a>
                            <form action="{{ route('dashboard.invoice.delete', ['id' => $invoice->id]) }}" method="post"
                                  style="margin-left: 12px;">
                                {{ csrf_field() }} {{ method_field('DELETE') }}
                                <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Yakin ingin menghapus data ?')">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </form>
                        </div>
                    </td>
                </tr>
            @empty
                <tr>
                    <td class="text-center" colspan="8">No invoice yet. <a href="#">Create new one</a>.</td>
                </tr>
            @endforelse
            </tbody>
        </table>
        <div class="col-md-12 col-xs-12" style="margin-top: 16px;">
            <div class="float-right">
                {{ $invoices->appends(request()->except('page'))->links() }}
            </div>
        </div>
    </div>

@endsection
