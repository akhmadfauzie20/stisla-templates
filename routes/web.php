<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('guest')->get('/', function () {
    return view('welcome');
});

Route::prefix('/auth')->namespace('Auth')->group(function () {

    Route::get('/login', 'LoginController@showLoginForm')->name('login');
    Route::get('/logout', 'LoginController@logout')->middleware('auth')->name('logout');
    Route::post('/login', 'LoginController@login');

    Route::get('/register', 'RegisterController@showRegisterForm')->name('register');
    Route::post('/register', 'RegisterController@register');

});

Route::prefix('/dashboard')->namespace('Dashboard')->middleware('auth')->group(function () {

    Route::get('/', 'DashboardController@index')->name('dashboard.index');

    Route::prefix('/invoice')->group(function () {
        Route::get('/', 'InvoiceController@index')->name('dashboard.invoice.index');
        Route::get('/form/{id?}', 'InvoiceController@form')->name('dashboard.invoice.form');
        Route::get('/print/{id}', 'InvoiceController@invoice')->name('dashboard.invoice.print');
        Route::post('/create', 'InvoiceController@create')->name('dashboard.invoice.create');
        Route::post('/update/{id}', 'InvoiceController@update')->name('dashboard.invoice.update');
        Route::delete('/delete/{id}', 'InvoiceController@delete')->name('dashboard.invoice.delete');
    });

    Route::prefix('/item')->group(function () {
        Route::post('/create', 'ItemController@create')->name('dashboard.item.create');
        Route::post('/update/{id}', 'ItemController@update')->name('dashboard.item.update');
        Route::delete('/delete/{id}', 'ItemController@delete')->name('dashboard.item.delete');
    });
});
